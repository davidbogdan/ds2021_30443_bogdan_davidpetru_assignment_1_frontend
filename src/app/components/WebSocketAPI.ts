import { AppComponent } from '../app.component';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import { UserAuth } from '../model/userauth';
import { MonitoredDataQueue } from '../model/monitoreddataqueue';
import { MonitoredData } from '../model/monitoreddata';
import { JsonpInterceptor } from '@angular/common/http';
import {environment} from 'src/environments/environment';
export class WebSocketAPI {
    webSocketEndPoint: string = `${environment.apiURL}/ws`;//'http://localhost:8080/ws';
    topic: string = "/topic/greetings";
    stompClient: any;
    appComponent: AppComponent;
    constructor(appComponent: AppComponent){
        this.appComponent = appComponent;
    }
    _connect() {
        console.log("Initialize WebSocket Connection");
        let ws = new SockJS(this.webSocketEndPoint);
        this.stompClient = Stomp.over(ws);
        const _this = this;
        _this.stompClient.connect({}, function () {
            _this.stompClient.subscribe(_this.topic, function (sdkEvent: any) {
              //  console.log("PULA");
                _this.onMessageReceived(sdkEvent);
            });
            //_this.stompClient.reconnect_delay = 2000;
        }, this.errorCallBack);
    };

    _disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
        }
        console.log("Disconnected");
    }

    // on error, schedule a reconnection attempt
    errorCallBack(error: string) {
        console.log("errorCallBack -> " + error)
        setTimeout(() => {
            this._connect();
        }, 5000);
    }

	/**
	 * Send message to sever via web socket
	 * @param {*} message 
	 */
    _send(message: any) {
        console.log("calling logout api via web socket");
        this.stompClient.send("/app/hello", {}, JSON.stringify(message));
    }

    onMessageReceived(message: any) {
        console.log("Message Recieved from Server :: " + message);
        var objectData : MonitoredDataQueue;
        objectData = JSON.parse(message.body)
        this.appComponent.handleMessage(objectData);
    }
}