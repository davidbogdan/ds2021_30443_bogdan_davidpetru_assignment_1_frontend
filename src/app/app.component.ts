import { Component } from '@angular/core';
import { ActivatedRoute, RouteConfigLoadStart, Router } from '@angular/router';
import { UserAuth } from './model/userauth';
import { AuthService } from './security/service/auth.service';
import { UsersService } from './services/user/users.service';
import { WebSocketAPI } from './components/WebSocketAPI';
import { MonitoredDataQueue } from './model/monitoreddataqueue';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'energytool';
  currentUser!:UserAuth;
  isAdmin!:boolean;
  loading = false;
  webSocketAPI?: WebSocketAPI;
  greeting: any;
  name: string;

  ngOnInit(){
    this.webSocketAPI = new WebSocketAPI(new AppComponent(this.router,this.loginService,this.userService,this.route));
    this.connect();
  }

  constructor(
    private router : Router,
    private loginService : AuthService,
    private userService: UsersService,
    private route: ActivatedRoute,
  ){
    this.name="fas";
    // this.webSocketAPI = new WebSocketAPI(new AppComponent(this.router,this.loginService,this.userService,this.route));
    // this.connect();
    this.loginService.currentUser.subscribe(user=>{
      this.currentUser=user;
      if(user && user.role){
        if( user.role=='ADMIN'){
          this.isAdmin=true;
        }
        else{
          this.isAdmin=false;
        }
      } 
    });
  }

  // ngOnInit() {
    
  // }


  logout():void{
    this.loginService.logout();
  }
  connect(){
    this.webSocketAPI?._connect();
  }

  disconnect(){
    this.webSocketAPI?._disconnect();
  }

  sendMessage(){
    this.webSocketAPI?._send(this.name);
  }

  // handleMessage(message:any){
  //   alert(message);
  //   this.greeting = message;
  // }

  handleMessage(message:MonitoredDataQueue){
    if(message.ownerUid==this.currentUser.uid){
      alert(message.message);
      this.greeting = message.message;
    }
  }
}
