
import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth.service';
import { environment } from 'src/environments/environment'

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      if (!this.checkIfUrlInWhiteList(request)) {
          return next.handle(request);
      }
      if (this.authenticationService.currentUserValue.jwt) {
          request = request.clone({
              setHeaders: {
                  Authorization:`Bearer ${this.authenticationService.currentUserValue.jwt}`
              }
          });
      }
      return next.handle(request);
  }

  checkIfUrlInWhiteList(request: HttpRequest<any>): boolean {
      let valid = true;

      environment.whiteList.forEach(urls => {
          if (request.url.endsWith(urls)) {
              valid = false;
          }
      });

      return valid;
  }
}