import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {RouterModule} from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MessagesComponent } from './components/messages/messages.component'
import { UsersComponent } from './pages/user/users/users.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { CreateUserComponent } from './pages/user/create-user/create-user.component';
import { UserDetailComponent } from './pages/user/user-detail/user-detail.component';
import { SensorsComponent } from './pages/sensor/sensors/sensors.component';
import { CreateSensorComponent } from './pages/sensor/create-sensor/create-sensor.component';
import { SensorDetailComponent } from './pages/sensor/sensor-detail/sensor-detail.component';
import { LoginComponent } from './security/login/login.component';
import { JwtInterceptor } from './security/interceptors/jwt.interceptor';
import { ErrorInterceptor } from './security/interceptors/error.interceptor';
import { DeviceComponent } from './pages/device/devices/devices.component';
import { CreateDeviceComponent } from './pages/device/create-device/create-device.component';
import { DeviceDetailsComponent } from './pages/device/device-details/device-details.component';
import { ClientInfoComponent } from './pages/client-info/client-info.component';
import { CreateMonitoreddataComponent } from './pages/create-monitoreddata/create-monitoreddata.component';
import { DataDetailsComponent } from './pages/data-details/data-details.component';
import { RegisterComponent } from './security/register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    UsersComponent,
    DashboardComponent,
    CreateUserComponent,
    UserDetailComponent,
    SensorsComponent,
    CreateSensorComponent,
    SensorDetailComponent,
    LoginComponent,
    DeviceComponent,
    CreateDeviceComponent,
    DeviceDetailsComponent,
    ClientInfoComponent,
    CreateMonitoreddataComponent,
    DataDetailsComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
