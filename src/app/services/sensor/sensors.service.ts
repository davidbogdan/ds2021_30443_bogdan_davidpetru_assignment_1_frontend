import { Injectable } from '@angular/core';
import {Sensor} from 'src/app/model/sensor';
import {Observable, of} from 'rxjs';
import {MessageService} from 'src/app/services/message.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import { MonitoredData } from 'src/app/model/monitoreddata';
@Injectable({
  providedIn: 'root'
})
export class SensorsService {

  private sensorsUrl = `${environment.apiURL}/sensors`;

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`SensorsService: ${message}`);
  }

  constructor(private http: HttpClient, private messageService: MessageService) {
  }

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  getSensors(): Observable<Sensor[]> {
    // const trainingSessions= of(TRAININGS);
    //this.messageService.add('TrainingService: fetched trainings');
    //return trainingSessions;
    return this.http.get<Sensor[]>(this.sensorsUrl)
      .pipe(
        tap(_ => this.log('fetched sensors')),
        catchError(this.handleError<Sensor[]>('getSensors', []))
      )
      ;
  }

  /** GET hero by id. Will 404 if id not found */
  getSensor(id: String): Observable<Sensor> {
    const url = `${this.sensorsUrl}/${id}`;
    return this.http.get<Sensor>(url).pipe(
      tap(_ => this.log(`fetched sensor id=${id}`)),
      catchError(this.handleError<Sensor>(`getSensor id=${id}`))
    );
  }

  /** PUT: update the hero on the server */
  updateSensor(sensor: Sensor): Observable<any> {
    const url = `${this.sensorsUrl}/${sensor.uid}`;
    return this.http.put(url, sensor, this.httpOptions).pipe(
      tap(_ => this.log(`updated sensor id=${sensor.uid}`)),
      catchError(this.handleError<any>('updateSensor'))
    );
  }

  /** POST: add a new hero to the server */
  addSensor(sensor: Sensor): Observable<Sensor> {
    return this.http.post<Sensor>(this.sensorsUrl, sensor, this.httpOptions).pipe(
      tap((newHero: Sensor) => this.log(`added sensor w/ id=${newHero.uid}`)),
      catchError(this.handleError<Sensor>('addSensor'))
    );
  }
  addMonitoredData(monitoreddata: MonitoredData):Observable<MonitoredData>{
    const url = `${this.sensorsUrl}/${monitoreddata.sensorUid}/monitored-data`;
    return this.http.post<MonitoredData>(url, monitoreddata, this.httpOptions).pipe(
      tap((newHero: MonitoredData) => this.log(`added monitored data to sensor w/ id=${newHero.sensorUid}`)),
      catchError(this.handleError<MonitoredData>('addMonitoredData'))
    );
  }
  getMonitoredData(id:String):Observable<MonitoredData[]>{
    const url = `${this.sensorsUrl}/${id}/monitored-data`;
    return this.http.get<MonitoredData[]>(url).pipe(
      tap(_ => this.log(`fetched monitoredData for sensor with id=${id}`)),
      catchError(this.handleError<MonitoredData[]>(`getData for sensor with id=${id}`))
    );
  }

  deleteSensor(id: String): Observable<Sensor> {
    const url = `${this.sensorsUrl}/${id}`;

    return this.http.delete<Sensor>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted sensor id=${id}`)),
      catchError(this.handleError<Sensor>('deleteSensor'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
