import { Injectable } from '@angular/core';
import {User} from 'src/app/model/user';
import {Observable, of} from 'rxjs';
import {MessageService} from 'src/app/services/message.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private usersUrl = `${environment.apiURL}/users`;

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`UsersService: ${message}`);
  }

  constructor(private http: HttpClient, private messageService: MessageService) {
  }

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  getUsers(): Observable<User[]> {
    // const trainingSessions= of(TRAININGS);
    //this.messageService.add('TrainingService: fetched trainings');
    //return trainingSessions;
    return this.http.get<User[]>(this.usersUrl)
      .pipe(
        tap(_ => this.log('fetched users')),
        catchError(this.handleError<User[]>('getUsers', []))
      )
      ;
  }

  /** GET hero by id. Will 404 if id not found */
  getUser(id: String): Observable<User> {
    const url = `${this.usersUrl}/${id}`;
    return this.http.get<User>(url).pipe(
      tap(_ => this.log(`fetched user id=${id}`)),
      catchError(this.handleError<User>(`getUser id=${id}`))
    );
  }

  /** PUT: update the hero on the server */
  updateUser(user: User): Observable<any> {
    const url = `${this.usersUrl}/${user.uid}`;
    return this.http.put(url, user, this.httpOptions).pipe(
      tap(_ => this.log(`updated user id=${user.uid}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

  /** POST: add a new hero to the server */
  addUser(user: User): Observable<User> {
    return this.http.post<User>(this.usersUrl, user, this.httpOptions).pipe(
      tap((newHero: User) => this.log(`added user w/ id=${newHero.uid}`)),
      catchError(this.handleError<User>('addUser'))
    );
  }

  deleteUser(id: String): Observable<User> {
    const url = `${this.usersUrl}/${id}`;

    return this.http.delete<User>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted user id=${id}`)),
      catchError(this.handleError<User>('deleteUser'))
    );
  }
  signIn(user: User): Observable<User> {
    const url = `${environment.apiURL}/signup`;
    return this.http.post<User>(url, user, this.httpOptions).pipe(
      tap((newHero: User) => this.log(`added user w/ id=${newHero.uid}`)),
      catchError(this.handleError<User>('addUser'))
    );
  }
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
