import { Device } from "./device";
import { Sensor } from "./sensor";

export interface ClientInfo {
    device:Device;
    sensor:Sensor;
  }