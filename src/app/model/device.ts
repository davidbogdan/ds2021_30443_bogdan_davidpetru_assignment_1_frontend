export interface Device {
    uid: String;
    description: String;
    location: String;
    maxConsumption: Number;
    averageConsumption: Number;
    ownerUid:String;
    sensorUid:String;
  }