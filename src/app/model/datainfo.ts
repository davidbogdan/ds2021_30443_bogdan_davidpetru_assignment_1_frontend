import { Device } from "./device";
import { MonitoredData } from "./monitoreddata";

export interface DataInfo{
    device:Device;
    monitoredDatas:MonitoredData[];
}