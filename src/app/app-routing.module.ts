import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { UsersComponent } from './pages/user/users/users.component';
import { CreateUserComponent } from './pages/user/create-user/create-user.component';
import { UserDetailComponent } from './pages/user/user-detail/user-detail.component';
import { SensorsComponent } from './pages/sensor/sensors/sensors.component';
import { CreateSensorComponent } from './pages/sensor/create-sensor/create-sensor.component';
import { SensorDetailComponent } from './pages/sensor/sensor-detail/sensor-detail.component';
import { LoginComponent } from './security/login/login.component';
import { AuthGuard } from './security/service/auth.guard';
import { AuthAdminGuard } from './security/service/auth-admin.guard';
import { DeviceComponent } from './pages/device/devices/devices.component';
import { CreateDeviceComponent } from './pages/device/create-device/create-device.component';
import { DeviceDetailsComponent } from './pages/device/device-details/device-details.component';
import { ClientInfoComponent } from './pages/client-info/client-info.component';
import { CreateMonitoreddataComponent } from './pages/create-monitoreddata/create-monitoreddata.component';
import { DataDetailsComponent } from './pages/data-details/data-details.component';
import { RegiterGuard } from './security/service/regiter.guard';
import { RegisterComponent } from './security/register/register.component';

const routes: Routes = [
  {path: 'users', component: UsersComponent, canActivate:[AuthAdminGuard]},
  {path: 'dashboard', component: DashboardComponent},
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'users/edit/:id', component: UserDetailComponent, canActivate:[AuthAdminGuard]},
  {path:'users/add',component: CreateUserComponent, canActivate:[AuthAdminGuard]},
  {path: 'register', component:RegisterComponent},
  {path: 'sensors',component: SensorsComponent, canActivate:[AuthAdminGuard]},
  {path: 'sensors/add',component:CreateSensorComponent, canActivate:[AuthAdminGuard]},
  {path: 'sensors/edit/:id', component: SensorDetailComponent, canActivate:[AuthAdminGuard]},
  {path: 'monitoreddata/add',component:CreateMonitoreddataComponent, canActivate:[AuthAdminGuard]},
  {path: 'login', component:LoginComponent},
  {path: 'devices',component: DeviceComponent,canActivate:[AuthAdminGuard]},
  {path: 'devices/add', component: CreateDeviceComponent,canActivate:[AuthAdminGuard]},
  {path: 'devices/edit/:id', component: DeviceDetailsComponent, canActivate:[AuthAdminGuard]},
  {path: 'clientinfo/:id',component: ClientInfoComponent, canActivate:[AuthGuard]},
  {path: 'datainfo/:id',component: DataDetailsComponent, canActivate:[AuthGuard]}
];
imports: [RouterModule.forRoot(routes)]
exports: [RouterModule]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
