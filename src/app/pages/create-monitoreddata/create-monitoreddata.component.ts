import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Device } from 'src/app/model/device';
import { DeviceService } from 'src/app/services/device/device.service';
import {Location} from '@angular/common';
import { User } from 'src/app/model/user';
import { UsersService } from 'src/app/services/user/users.service';
import { Sensor } from 'src/app/model/sensor';
import { SensorsService } from 'src/app/services/sensor/sensors.service';
import { MonitoredData } from 'src/app/model/monitoreddata';

@Component({
  selector: 'app-create-monitoreddata',
  templateUrl: './create-monitoreddata.component.html',
  styleUrls: ['./create-monitoreddata.component.css']
})
export class CreateMonitoreddataComponent implements OnInit {

  monitoredDataForm = new FormGroup({
    energyConsumption: new FormControl(0),
    date: new FormControl('', Validators.required),
    sensorUid: new FormControl('',Validators.required)
  })
  allSensors?:Sensor[];
  constructor(private sensorService: SensorsService, private location: Location) {
    sensorService.getSensors().subscribe(sensors=>this.allSensors=sensors);
  }

  selectedType?: string;

  onSubmit() {
    console.log(this.monitoredDataForm.value);
    const date = this.monitoredDataForm.value.date;
    const energyConsumption = this.monitoredDataForm.value.energyConsumption;
    const sensorUid = this.monitoredDataForm.value.sensorUid;
    this.sensorService.addMonitoredData({date,energyConsumption,sensorUid} as MonitoredData)
      .subscribe(monitoreddata => {
        this.location.back();
      });
  }

  ngOnInit(): void {
  }

}
