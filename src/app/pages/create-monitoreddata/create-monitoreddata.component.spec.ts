import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMonitoreddataComponent } from './create-monitoreddata.component';

describe('CreateMonitoreddataComponent', () => {
  let component: CreateMonitoreddataComponent;
  let fixture: ComponentFixture<CreateMonitoreddataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateMonitoreddataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMonitoreddataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
