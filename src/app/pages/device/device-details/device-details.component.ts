import { Component, OnInit,Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import { Device } from 'src/app/model/device';
import { DeviceService } from 'src/app/services/device/device.service';
import { User } from 'src/app/model/user';
import { Sensor } from 'src/app/model/sensor';
import { SensorsService } from 'src/app/services/sensor/sensors.service';
import { UsersService } from 'src/app/services/user/users.service';
@Component({
  selector: 'app-device-details',
  templateUrl: './device-details.component.html',
  styleUrls: ['./device-details.component.css']
})
export class DeviceDetailsComponent implements OnInit {

  @Input() device?: Device;
  allUsers?: User[];
  allAvailableSensors?:Sensor[];
  constructor(private deviceService: DeviceService,private sensorService: SensorsService,private userService: UsersService,private route: ActivatedRoute, private location: Location) {
  
  }

  save(): void {
    if (this.device) {
      this.deviceService.updateDevice(this.device)
        .subscribe(() => this.goBack());
    }

  }

  ngOnInit(): void {
    this.getDevice();
    this.userService.getUsers().subscribe(users=>this.allUsers=users);
    this.sensorService.getSensors().subscribe(sensors=>this.allAvailableSensors=sensors.filter(sensor=>sensor.deviceUid==null || sensor.deviceUid==this.device?.uid));
  }

  getDevice(): void {
    const id = String(this.route.snapshot.paramMap.get('id'));
    this.deviceService.getDevice(id)
      .subscribe(device => this.device = device);
  }

  goBack(): void {
    this.location.back();
  }

}
