import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Device } from 'src/app/model/device';
import { DeviceService } from 'src/app/services/device/device.service';
import {Location} from '@angular/common';
import { User } from 'src/app/model/user';
import { UsersService } from 'src/app/services/user/users.service';
import { Sensor } from 'src/app/model/sensor';
import { SensorsService } from 'src/app/services/sensor/sensors.service';

@Component({
  selector: 'app-create-device',
  templateUrl: './create-device.component.html',
  styleUrls: ['./create-device.component.css']
})
export class CreateDeviceComponent implements OnInit {
  deviceForm = new FormGroup({
    maxConsumption: new FormControl(0),
    averageConsumption: new FormControl(0),
    location: new FormControl('', Validators.required),
    ownerUid: new FormControl('',Validators.required),
    sensorUid: new FormControl('',Validators.required),
    description: new FormControl('', Validators.required)
  })
  allUsers?: User[];
  allAvailableSensors?:Sensor[];
  constructor(private deviceService: DeviceService,private sensorService: SensorsService,private userService: UsersService, private location: Location) {
    userService.getUsers().subscribe(users=>this.allUsers=users);
    sensorService.getSensors().subscribe(sensors=>this.allAvailableSensors=sensors.filter(sensor=>sensor.deviceUid==null));
  }

  selectedType?: string;

  onSubmit() {
    console.log(this.deviceForm.value);
    const description = this.deviceForm.value.description;
    const location = this.deviceForm.value.location;
    const maxConsumption = this.deviceForm.value.maxConsumption;
    const averageConsumption = this.deviceForm.value.averageConsumption;
    const ownerUid = this.deviceForm.value.ownerUid;
    const sensorUid = this.deviceForm.value.sensorUid;
    this.deviceService.addDevice({description,location,maxConsumption,averageConsumption,ownerUid,sensorUid} as Device)
      .subscribe(device => {
        this.location.back();
      });
  }

  ngOnInit(): void {
  }
//   private isAvailable(user:User, index, array) { 
//     return (element >= 10); 
//  } 

}
