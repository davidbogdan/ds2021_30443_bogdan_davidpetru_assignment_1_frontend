import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Sensor } from 'src/app/model/sensor';
import { SensorsService } from 'src/app/services/sensor/sensors.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-create-sensor',
  templateUrl: './create-sensor.component.html',
  styleUrls: ['./create-sensor.component.css']
})
export class CreateSensorComponent implements OnInit {
  sensorForm = new FormGroup({
    maxValue: new FormControl(1, [Validators.required, Validators.min(1)]),
    description: new FormControl('', Validators.required)
  })

  constructor(private sensorService: SensorsService, private location: Location) {
  }

  selectedType?: string;

  onSubmit() {
    console.log(this.sensorForm.value);
    const description = this.sensorForm.value.description;
    const max_value = this.sensorForm.value.maxValue;
    this.sensorService.addSensor({description, max_value} as Sensor)
      .subscribe(sensor => {
        this.location.back();
      });
  }

  ngOnInit(): void {
  }

}
