import { Component, OnInit,Input } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import { SensorsService } from 'src/app/services/sensor/sensors.service';
import { Sensor } from 'src/app/model/sensor';
@Component({
  selector: 'app-sensor-detail',
  templateUrl: './sensor-detail.component.html',
  styleUrls: ['./sensor-detail.component.css']
})
export class SensorDetailComponent implements OnInit {

  @Input() sensor?: Sensor;

  constructor(private sensorService: SensorsService,
              private route: ActivatedRoute,
              private location: Location) {
  }

  save(): void {
    if (this.sensor) {
      this.sensorService.updateSensor(this.sensor)
        .subscribe(() => this.goBack());
    }

  }

  ngOnInit(): void {
    this.getUser();
  }

  getUser(): void {
    const id = String(this.route.snapshot.paramMap.get('id'));
    this.sensorService.getSensor(id)
      .subscribe(sensor => this.sensor = sensor);
  }

  goBack(): void {
    this.location.back();
  }

}
