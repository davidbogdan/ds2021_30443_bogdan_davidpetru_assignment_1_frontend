export const environment = {
  production: true,
  apiURL: 'https://spring-demo-ds2021.herokuapp.com',
  whiteList: ['auth','signup']
};
